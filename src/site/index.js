function fetchQA(terms) {
    $.ajax({
        type: 'POST',
        url: '/search',
        data: JSON.stringify({
            terms,
        }),
        contentType: 'application/json',
        success: (qas) => {
            const list = $('#list');
            list.empty();
            for (let qa of qas) {
                list.append(`<li>${qa.question}: ${qa.answer}</li>`);
            }
        }
    });
}


function insertQA(question, answer) {
    $.ajax({
        type: 'POST',
        url: '/insert',
        data: JSON.stringify({
            question,
            answer,
        }),
        contentType: 'application/json',
        success: (res) => {
            $('#insert input[name="question"]').val('');
            $('#insert input[name="answer"]').val('');
            fetchQA('');
        },
    });
}

function login(username, password) {
    $.ajax({
        type: 'POST',
        url: '/login',
        data: JSON.stringify({
            username,
            password,
        }),
        contentType: 'application/json',
        success: (res) => {
            $('#login').remove();
        },
    });
}

fetchQA('');
$('#insert').on('submit', (event) => {
    event.preventDefault();
    const question = $('#insert input[name="question"]').val();
    const answer = $('#insert input[name="answer"]').val();
    insertQA(question, answer);
});
$('#login').on('submit', (event) => {
    event.preventDefault();
    const username = $('#login input[name="username"]').val();
    const password = $('#login input[name="password"]').val();
    login(username, password);
});
$('#search').on('submit', (event) => {
    event.preventDefault();
    const terms = $('#search input[name="terms"]').val();
    fetchQA(terms);
});