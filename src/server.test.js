const createServer = require('./server.js');
const assert = require('assert');
const axios = require('axios');

let cookie;

const post = async (endpoint, body) => {
    const headers = {};
    if (cookie) headers.cookie = cookie;
    const res = await axios.post(`http://localhost:3000/${endpoint}`, body, { headers });
    if (res.headers['set-cookie']) {
        cookie = res.headers['set-cookie'][0];
    }
    return res.data;
}

createServer(async function() {
    await post('register', { username: 'bob', password: 'jim' });
    console.log('Registered bob');

    await assert.rejects(post('insert', { question: 'Can I hack this API?', answer: 'No' }));
    console.log('Cannot hack this');

    await assert.rejects(post('login', { wrong: 'props' }));
    console.log('Cannot log in with invalid request');

    await assert.rejects(post('login', { username: 'haha', password: 'nobody' }));
    console.log('Cannot log in with non-existent account');

    await assert.rejects(post('login', { username: 'bob', password: 'wrongpassword' }));
    console.log('Cannot log in with wrong password');

    await post('login', { username: 'bob', password: 'jim' });
    console.log('Logged in as bob');

    await post('insert', { question: 'Do androids dream of electric sheep?', answer: 'Maybe' });
    console.log('Inserted a question');

    const rows = await post('search', { terms: 'androids' });
    console.log('Fetched a question', rows);
    assert(rows.length === 1);
    assert(rows[0].question === 'Do androids dream of electric sheep?');
    assert(rows[0].answer === 'Maybe');

    process.exit(0);
});