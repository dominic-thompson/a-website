const express = require('express');
const cookieParser = require('cookie-parser');
const sqlite = require('sqlite3');
const path = require('path');

const db = new sqlite.Database(process.env.NODE_ENV === 'test' ? ':memory:' : './website.db');

const app = express();

app.use(express.json());

app.use(cookieParser());

app.post('/register', (req, res) => {
    const { username, password } = req.body;
    db.run('insert into users (username, password) values (?, ?)', [username, password], (err) => {
        if (err) throw err;
        return res.json({ status: 'OK' });
    });
});

app.post('/login', (req, res) => {
    const { username, password } = req.body;
    if (!(username && password)) return res.status(400).json({ status: 'Bad Request' });
    db.get('select password from users where username = ?', [username], (err, row) => {
        if (err) throw err;
        if (row && password === row.password) {
            res.cookie('session', 'lol', { maxAge: 999999 });
            return res.json({ status: 'OK' });
        }
        return res.status(401).json({ status: 'Forbidden' });
    });
});

const checkLogin = (req, res, next) => {
    if (!(req.cookies && req.cookies.session && req.cookies.session === 'lol')) {
        return res.status(401).json({ status: 'Forbidden' });
    }
    next();
}

app.post('/insert', checkLogin, (req, res) => {
    const { question, answer } = req.body;
    if (!(question && answer)) return res.status(400).json({ status: 'Bad Request' });
    db.run('insert into qa (question, answer) values (?, ?)', [question, answer], (err) => {
        if (err) throw err;
        return res.json({ status: 'OK' });
    });
});

app.post('/search', checkLogin, (req, res) => {
    const { terms } = req.body;
    if (!terms) {
        db.all('select question, answer from qa', [], (err, rows) => {
            if (err) throw err;
            return res.json(rows);
        });    
    } else {
        db.all('select question, answer from qa where question like ? or answer like ?', [`%${terms}%`, `%${terms}%`], (err, rows) => {
            if (err) throw err;
            return res.json(rows);
        });
    }
});

app.use(express.static(path.join(__dirname, 'site')));

module.exports = function(cb) {
    db.run('create table if not exists users (id integer primary key, username text, password text)', (err) => {
        if (err) throw err;
        db.run('create table if not exists qa (id integer primary key, question text, answer text)', (err) => {
            if (err) throw err;
            app.listen(3000, () => {
                console.log('Server started on port 3000');
                if (cb) cb();
            });
        }); 
    });
}